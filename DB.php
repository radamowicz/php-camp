<?php



interface iDB {

    public function __construct($host, $login, $password, $dbName, $port);

    public function query($query);

    public function getAffectedRows();

    public function getRow();

    public function getAllRows();
}

class DB implements iDB
{
    private $link;
    private $queryResult;

    public function __construct($host, $login, $password, $dbName, $port)
    {
        $this->link = mysqli_connect($host, $login, $password, $dbName, $port);
        if (!$this->link) {
            throw new Exception ('Nie udalo sie polaczyc z baza danych: ' . $dbName);
        }
    }



    public function query($query)
    {
        $this->queryResult = mysqli_query($this->link, $query);

        return (bool)$this->queryResult;
    }


    public function getAffectedRows()
    {
        return mysqli_affected_rows($this->link);
    }


    public function getRow()
    {
        if (!$this->queryResult) {
            throw new Exception('Zapytanie nie zostalo wykonane');
        }

        return mysqli_fetch_assoc($this->queryResult);
    }


    public function getAllRows()
    {
        $results = array();

        while (($row = $this->getRow())) {
            $results[] = $row;
        }

        return $results;
    }
}

try {
    $DB = new DB('127.0.0.1', 'root', '9ds2ef8a', 'test', 3308);
    $DB->query('SELECT * FROM clients WHERE name LIKE "A%"');
    var_dump($DB->getAllRows());
} catch (Exception $e) {
    var_dump($e->getMessage());
}


print 'end';